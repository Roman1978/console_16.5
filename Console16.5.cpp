﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>
using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");
	const int n = 5;
	const int m = 5;

	int a[n][m];

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			a[i][j] = i + j;
		}
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			cout << a[i][j] << "\t";
		}
		cout << endl;
	}

	int N, sum = 0;
	cout << "Введите значение N: ";
	cin >> N;
	{
		time_t t;
		time(&t);
		int k = (localtime(&t)->tm_mday) % N;

		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
			{
				if (i == k) sum += a[i][j];
			}
		cout << "Сумма элементов строки " << k << " = " << sum;		
	}
	return 0;
}